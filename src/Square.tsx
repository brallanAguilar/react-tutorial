//import React, {Component} from "react"; // 'React' is the default export, 'Component' is a named one (https://stackoverflow.com/a/36796281/1226831)
import React from "react";

// A React component has two parts: properties and states.
// Properties (props) are variables passed to the instance when is created. :
//      - When instancing a component: <ChildComponent color=green />, 'color' is a property.
//      - Property Updates May Be Asynchronous (https://reactjs.org/docs/state-and-lifecycle.html#state-updates-may-be-asynchronous)

// State contains data that may change while the instance exists, it's a plain JS object (so, could be a function).
// Usage (https://reactjs.org/docs/state-and-lifecycle.html#using-state-correctly):
//      - Mutate it using setState only.
//      - State Updates May Be Asynchronous (to "fix" that, use a function: https://reactjs.org/docs/state-and-lifecycle.html#state-updates-may-be-asynchronous).
//      - State Updates are Merged (it's an object, so any new state in that object is appended, if a state already exists, is updated. https://reactjs.org/docs/state-and-lifecycle.html#state-updates-are-merged).

// Props vs state (https://stackoverflow.com/a/33115945/1226831)
// Comes down to "who owns this data?"
//      - If data is managed by one component, but another component needs access to that data, you'd pass the data
//        from the one component to the other component via props.
//      - If a component manages the data itself, it should use state and setState to manage it.

// export (https://www.typescriptlang.org/docs/handbook/modules.html#exporting-a-declaration)
// union (https://www.typescriptlang.org/docs/handbook/advanced-types.html#union-types)
export type SquareText = 'X' | 'O' | null;

interface SquareProp  { // interface is used to define a data container (as a JS object), nothing related with polymorphism terminology
    value: SquareText
    onClick(): void // function type https://www.typescriptlang.org/docs/handbook/interfaces.html#function-types
}

/* Using Square as a class

interface SquareState {
    text: SquareText
}

// The Component (React.Component) TypeScript version receives two types: properties and state.
class Square extends Component<SquareProp, SquareState> {
    constructor(props: SquareProp) {
        super(props);
        this.state = {
            text: null
        };
    }
    render() {
        return (
            <button
                className="square"
                onClick={ () => this.props.onClick() }
            >
                { this.props.id }
            </button>
        );
    }
}
*/

// Using "Function components"
function Square(props: SquareProp) {
    return (
        <button
            className="square"
            onClick={ props.onClick } // same as onClick={() => props.onClick()}
        >
            { props.value }
        </button>
    );
}

export default Square;
